@webhooks @azure @log-azure-server-data-webhook @needs-manual-config
Feature: Determine Azure scale set server data based on webhook data.
  In order to process Azure scale set webhooks
  As a developer
  I need to log server data from Azure scale sets.

  Background:
    Given I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  Scenario: Call Azure scale in log server webhook script.
     When I run "./scripts/server.sh log_azure_server_data azure_server_data_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/log_azure_server_data?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:log_azure_server_data
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       log_azure_server_data - azure_server_data_payload.json - localhost - 127.0.0.1 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       LogAzureServerDataProcessor webhook plugin succeeded for Scale In operation
       for the aegirWebhookTestScaleSet scale set.
       LogAzureServerDataProcessor webhook plugin triggered for Scale In operation
       for the aegirWebhookTestScaleSet scale set.
       """
