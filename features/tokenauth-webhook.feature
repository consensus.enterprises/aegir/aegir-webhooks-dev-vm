@webhooks @tokenauth-webhook
Feature: Testing token authorization webhook
  In order to secure webhooks
  As a developer
  I need to process an authorization token.

  Background:
    Given I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  Scenario: Call token authorization webhook script with correct (default) token.
     When I run "./scripts/tokenauth.sh example_tokenauth tokenauth_payload.json localhost 127.0.0.1 90437cca-aa3b-4b07-8ce3-db95826de868"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/example_tokenauth?authorization_token=90437cca-aa3b-4b07-8ce3-db95826de868 HTTP/1.1
       > Host: aegir.local
       > User-Agent: Aegir-test/example
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       example_tokenauth - tokenauth_payload.json - localhost - 127.0.0.1 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       Webhook received (example_tokenauth). Message: TokenAuthorization unserializer
       plugin succeeded.
       """

  @tokenauth-webhook-notoken
  Scenario: Call token authorization webhook script with missing token.
     When I run "./scripts/logdata.sh example_tokenauth tokenauth_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/example_tokenauth HTTP/1.1
       > Host: aegir.local
       > User-Agent: Aegir-test/example
       * The requested URL returned error: 401 Unauthorized
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       example_tokenauth - tokenauth_payload.json - localhost - 127.0.0.1 - RC: 22
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       Expected authorization token missing from query parameters for
       example_tokenauth webhook.
       """

  @tokenauth-webhook-badtoken
  Scenario: Call token authorization webhook script with invalid token.
     When I run "./scripts/tokenauth.sh example_tokenauth tokenauth_payload.json localhost 127.0.0.1 intentionallyInvalid"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/example_tokenauth?authorization_token=intentionallyInvalid HTTP/1.1
       > Host: aegir.local
       > User-Agent: Aegir-test/example
       * The requested URL returned error: 401 Unauthorized
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       example_tokenauth - tokenauth_payload.json - localhost - 127.0.0.1 - RC: 22
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       Authorization token (intentionallyInvalid) invalid for example_tokenauth
       webhook.
       """
