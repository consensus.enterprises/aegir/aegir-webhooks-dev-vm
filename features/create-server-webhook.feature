@webhooks @server @create-server-webhook
Feature: Webhook triggers creation of an Aegir server.
  In order to create servers in response to cloud provisioning events
  As a developer
  I need create_server webhooks to create Aegir servers.

  Background:
    # Clean up any cruft from previous test runs.
    Given I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster php-script /vagrant/scripts/delete_servers.php\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster variable-set hosting_webhooks_check_ssh 0\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

# @TODO Add test for graceful failure when server already exists
# @TODO Add test to ensure server node is reused (currently not implemented)?

  Scenario: Call create_server webhook.
     When I run "./scripts/server.sh create_server testhost_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/create_server?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:create_server
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       create_server - testhost_payload.json - localhost - 127.0.0.1 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       CreateServerProcessor webhook plugin succeeded for test.host.
       No services defined for test.host
       CreateServerProcessor webhook plugin triggered for test.host.
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-tasks\"'
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa\"'
     Then I should get:
       """
       @server_testhost
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa @server_testhost\"'
     Then I should get:
       """
       $aliases["server_testhost"] = array (
         'context_type' => 'server',
         'remote_host' => 'test.host',
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @server_testhost provision-verify || /bin/true\"'
     Then I should not get:
       """
       [error]
       Could not rsync from '/var/aegir/config/includes'
       """
