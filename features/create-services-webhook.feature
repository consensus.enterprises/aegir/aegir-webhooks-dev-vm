@webhooks @create-services-webhook
Feature: Webhook triggers creation of an Aegir server with defined services.
  In order to create useful servers in response to cloud provisioning events
  As a developer
  I need create_server webhooks to create Aegir servers with properly configured services.

  Background:
    # Clean up any cruft from previous test runs.
    Given I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster php-script /vagrant/scripts/delete_servers.php\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster variable-set hosting_webhooks_check_ssh 0\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  # @TODO: Add test of graceful handling of unrecognized service.
  # @TODO: Add tests spinning up and down Vagrant VMs so that `provision-verify` succeeds.

  Scenario: Call create_server webhook with a payload that includes services.
     When I run "./scripts/server.sh create_server serviceshost_payload.json localhost 127.0.0.1"
      And I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       create_server - serviceshost_payload.json - localhost - 127.0.0.1 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       CreateServerProcessor webhook plugin succeeded for services.host.
       Adding apache Web service for services.host.
       CreateServerProcessor webhook plugin triggered for services.host.
       """
      And I should not get:
       """
       No services defined for services.host
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-tasks\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa @server_serviceshost\"'
     Then I should get:
       """
       $aliases["server_serviceshost"] = array (
         'context_type' => 'server',
         'remote_host' => 'services.host',
         'http_service_type' => 'apache',
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @server_serviceshost provision-verify || /bin/true\"'
     Then I should get:
       """
       [error]
       Could not rsync from '/var/aegir/config/includes'
       """
