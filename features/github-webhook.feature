@webhooks @github-webhook
Feature: Testing sample webhook
  In order to test webhooks consistently
  As a developer
  I need to trigger webhooks via a script

  Background:
    Given I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  Scenario: Call sample github webhook script.
     When I run "./scripts/github.sh github github_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/github HTTP/1.1
       > Host: aegir.local
       > User-Agent: GitHub-Hookshot/github
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       github - github_payload.json - localhost - 127.0.0.1 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=webhook_github_logger\"'
     Then I should get:
       """
       New push to Hello-World/refs/tags/simple-tag by Codertocat
       (21031067+Codertocat@users.noreply.github.com). View diff on github.
       """
