@webhooks @webpack @remove-webpack-server-webhook
Feature: Webhook triggers removal of an Aegir server from a webpack.
  In order to remove servers from a webpack in response to cloud provisioning events
  As a developer
  I need to handle remove_webpack_server webhooks properly.

  Background:
    # Clean up any cruft from previous test runs.
    Given I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster php-script /vagrant/scripts/delete_servers.php\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  Scenario: Call remove_webpack_server webhook for a server that does not exist.
     When I run "./scripts/server.sh remove_webpack_server webpackhost_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/remove_webpack_server?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:remove_webpack_server
       * The requested URL returned error: 500 Internal Server Error
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       remove_webpack_server - webpackhost_payload.json - localhost - 127.0.0.1 - RC: 22
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       Could not remove server from webpack. Error message: No server matched
       hostname.
       RemoveWebpackServerProcessor webhook plugin triggered for webpack.host with
       pack cluster drupal-webpack.
       """
     Then I should not get:
       """
       RemoveWebpackServerProcessor webhook plugin succeeded for webpack.host with
       """

  Scenario: Call remove_webpack_server webhook for a server that was already deleted.
    Given I run 'vagrant ssh aegir --command=\"sudo -sHu aegir cp /vagrant/scripts/server_webpackhost.alias.drushrc.php /var/aegir/.drush\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-import @server_webpackhost\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-tasks\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-task @server_webpackhost delete\"'
      And I run 'sleep 3'
     When I run "./scripts/server.sh remove_webpack_server webpackhost_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/remove_webpack_server?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:remove_webpack_server
       * The requested URL returned error: 500 Internal Server Error
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       remove_webpack_server - webpackhost_payload.json - localhost - 127.0.0.1 - RC: 22
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       Could not remove server from webpack. Error message: No server matched
       hostname.
       RemoveWebpackServerProcessor webhook plugin triggered for webpack.host with
       pack cluster drupal-webpack.
       """
     Then I should not get:
       """
       RemoveWebpackServerProcessor webhook plugin succeeded for webpack.host with
       """

  Scenario: Call remove_webpack_server webhook for a server that exists.
    Given I run 'vagrant ssh aegir --command=\"sudo -sHu aegir cp /vagrant/scripts/server_webpackhost.alias.drushrc.php /var/aegir/.drush\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-import @server_webpackhost\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-tasks\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir cp /vagrant/scripts/server_drupalwebpack.alias.drushrc.php /var/aegir/.drush\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-import @server_drupalwebpack\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-tasks\"'
      And I run 'sleep 3'
     When I run "./scripts/server.sh remove_webpack_server webpackhost_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/remove_webpack_server?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:remove_webpack_server
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       remove_webpack_server - webpackhost_payload.json - localhost - 127.0.0.1 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       RemoveWebpackServerProcessor webhook plugin succeeded for webpack.host with
       pack cluster drupal-webpack.
       RemoveWebpackServerProcessor webhook plugin triggered for webpack.host with
       pack cluster drupal-webpack.
       """
     Then I should not get:
       """
       Could not determine which server to delete.
       """
# @TODO: Check that alias file is gone
