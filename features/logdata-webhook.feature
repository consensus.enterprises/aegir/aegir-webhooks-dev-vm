@webhooks @logdata-webhook
Feature: Testing example webhook
  In order to test webhooks consistently
  As a developer
  I need to log webhook data to the database

  Background:
    Given I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  Scenario: Call example logdata webhook script.
     When I run "./scripts/logdata.sh example_logdata logdata_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/example_logdata HTTP/1.1
       > Host: aegir.local
       > User-Agent: Aegir-test/example
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       example_logdata - logdata_payload.json - localhost - 127.0.0.1 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       Webhook received (example_logdata). Message: LogDataProcessor plugin
       succeeded.
       LogDataProcessor webhook plugin triggered.
       """
