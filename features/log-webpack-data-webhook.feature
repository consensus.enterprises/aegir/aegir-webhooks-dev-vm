@webhooks @webpack @log-webpack-data-webhook
Feature: Validate incoming webpack webhook data.
  In order to test webpack webhooks consistently
  As a developer
  I need to log incoming webpack webhook data to the database.

  Background:
    Given I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  @direct
  Scenario: Call webpack log webhook script directly.
     When I run "./scripts/server.sh log_webpack_data webpackhost_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/log_webpack_data?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:log_webpack_data
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       log_webpack_data - webpackhost_payload.json - localhost - 127.0.0.1 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       LogWebpackDataProcessor webhook plugin succeeded for webpack.host with pack
       cluster drupal-webpack.
       LogWebpackDataProcessor webhook plugin triggered for webpack.host with pack
       cluster drupal-webpack.
       """
