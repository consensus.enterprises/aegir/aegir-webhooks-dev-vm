@webhooks @webpack @add-webpack-server-webhook
Feature: Webhook triggers adding an Aegir server to a webpack.
  In order to register servers in clusters in response to cloud provisioning events
  As a developer
  I need add_webpack_server webhooks to add Aegir servers to webpacks.

  Background:
    # Clean up any cruft from previous test runs.
    Given I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster php-script /vagrant/scripts/delete_servers.php\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster variable-set hosting_webhooks_check_ssh 0\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  Scenario: Call add_webpack_server webhook.
     When I run "./scripts/server.sh add_webpack_server webpackhost_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/add_webpack_server?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:add_webpack_server
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       add_webpack_server - webpackhost_payload.json - localhost - 127.0.0.1 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       AddWebpackServerProcessor webhook plugin succeeded for webpack.host with pack
       cluster drupal-webpack.
       AddWebpackServerProcessor webhook plugin triggered for webpack.host with pack
       cluster drupal-webpack.
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-tasks\"'
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa\"'
     Then I should get:
       """
       @server_webpackhost
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa @server_webpackhost\"'
     Then I should get:
       """
       $aliases["server_webpackhost"] = array (
         'context_type' => 'server',
         'remote_host' => 'webpack.host',
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @server_webpackhost provision-verify || /bin/true\"'
     Then I should not get:
       """
       [error]
       Could not rsync from '/var/aegir/config/includes'
       """
# TODO figure out what this should look like. presumably there's a service type, or similar... maybe an array of member servers?
#     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa\"'
#     Then I should get:
#       """
#       @server_drupalwebpack
#       """
#     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa @server_drupalwebpack\"'
#     Then I should get:
#       """
#       $aliases["server_drupalwebpack"] = array (
#         'context_type' => 'server',
#         'remote_host' => 'webpack.host',
#       """
#     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @server_drupalwebpack provision-verify || /bin/true\"'
#     Then I should not get:
#       """
#       [error]
#       Could not rsync from '/var/aegir/config/includes'
#       """
