@webhooks @server @log-server-data-webhook
Feature: Validate incoming server webhook data.
  In order to test server webhooks consistently
  As a developer
  I need to log incoming server webhook data to the database.

  Background:
    Given I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  @direct
  Scenario: Call server log webhook script directly.
     When I run "./scripts/server.sh log_server_data testhost_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/log_server_data?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:log_server_data
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       log_server_data - testhost_payload.json - localhost - 127.0.0.1 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       LogServerDataProcessor webhook plugin succeeded for test.host.
       LogServerDataProcessor webhook plugin triggered for test.host.
       """

  @vagrant-trigger
  Scenario: Call server log webhook script via Vagrant trigger.
     When I run 'VAGRANT_EXPERIMENTAL=\"typed_triggers\" vagrant status'
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/log_server_data?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:log_server_data
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       log_server_data - web0_payload.json - web0.local - 10.55.55.60 - RC: 0
       log_server_data - web1_payload.json - web1.local - 10.55.55.70 - RC: 0
       log_server_data - web2_payload.json - web2.local - 10.55.55.80 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       LogServerDataProcessor webhook plugin succeeded for web0.local.
       LogServerDataProcessor webhook plugin triggered for web0.local.
       LogServerDataProcessor webhook plugin succeeded for web1.local.
       LogServerDataProcessor webhook plugin triggered for web1.local.
       LogServerDataProcessor webhook plugin succeeded for 10.55.55.80.
       LogServerDataProcessor webhook plugin triggered for 10.55.55.80.
       """
