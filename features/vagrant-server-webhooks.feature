@webhooks @server @vagrant-server-webhooks @slow
Feature: Vagrant state changes trigger webhooks to create and delete Aegir servers.
  In order to create useful servers in response to cloud provisioning events
  As a developer
  I need Vagrant events to trigger webhooks.

  Background:
    # Clean up any cruft from previous test runs.
    Given I run "vagrant up web0"
      And I run "vagrant suspend web0 web1 web2"
      And I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster php-script /vagrant/scripts/delete_servers.php\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  Scenario: End-to-end Aegir server creation, verification and deletion.
     When I run "vagrant resume web0"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/create_server?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:create_server
       < HTTP/1.1 200 OK
       """
      And I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       create_server - web0_payload.json - web0.local - 10.55.55.60 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       CreateServerProcessor webhook plugin succeeded for web0.local.
       Adding apache Web service for web0.local.
       CreateServerProcessor webhook plugin triggered for web0.local.
       """
      And I should not get:
       """
       No services defined for web0.local
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-tasks\"'
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa\"'
     Then I should get:
       """
       @server_web0local
       """
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa @server_web0local\"'
     Then I should get:
       """
       $aliases["server_web0local"] = array (
         'context_type' => 'server',
         'remote_host' => 'web0.local',
         'http_service_type' => 'apache',
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @server_web0local provision-verify\"'
     Then I should not get:
       """
       [error]
       """
     When I run "vagrant suspend web0"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/delete_server?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:delete_server
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       delete_server - web0_payload.json - web0.local - 10.55.55.60 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       DeleteServerProcessor webhook plugin succeeded for web0.local.
       DeleteServerProcessor webhook plugin triggered for web0.local.
       """
     Then I should not get:
       """
       Could not determine which server to delete.
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-tasks\"'
      And I run 'sleep 10'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa\"'
     Then I should not get:
       """
       @server_web0local
       """
