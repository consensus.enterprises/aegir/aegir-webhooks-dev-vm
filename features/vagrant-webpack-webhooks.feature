@webhooks @webpack @vagrant-webpack-webhooks @slow
Feature: Vagrant state changes trigger webhooks to add/remove Aegir servers to/from webpacks.
  In order to create useful webpacks in response to cloud provisioning events
  As a developer
  I need Vagrant events to trigger webhooks.

  Background:
    # Clean up any cruft from previous test runs.
    Given I run "vagrant up web1 web2"
    Given I run "vagrant suspend web1 web2"
      And I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster php-script /vagrant/scripts/delete_servers.php\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  Scenario: End-to-end Aegir server creation, verification and deletion.
     When I run "vagrant resume web1 web2"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/add_webpack_server?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:add_webpack_server
       < HTTP/1.1 200 OK
       """
      And I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       add_webpack_server - web1_payload.json - web1.local - 10.55.55.70 - RC: 0
       add_webpack_server - web2_payload.json - web2.local - 10.55.55.80 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       AddWebpackServerProcessor webhook plugin succeeded for 10.55.55.80 with pack
       cluster drupal0.
       Adding apache Web service for 10.55.55.80.
       AddWebpackServerProcessor webhook plugin triggered for 10.55.55.80 with pack
       cluster drupal0.
       AddWebpackServerProcessor webhook plugin succeeded for web1.local with pack
       cluster drupal0.
       Adding apache Web service for web1.local.
       AddWebpackServerProcessor webhook plugin triggered for web1.local with pack
       cluster drupal0.
       """
      And I should not get:
       """
       No services defined for web1.local
       No services defined for 10.55.55.80
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-tasks\"'
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa\"'
     Then I should get:
       """
       @server_web1local
       @server_10555580
       @server_drupal0
       """
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa @server_web1local\"'
     Then I should get:
       """
       $aliases["server_web1local"] = array (
         'context_type' => 'server',
         'remote_host' => 'web1.local',
         'http_service_type' => 'apache',
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @server_web1local provision-verify\"'
     Then I should not get:
       """
       [error]
       """
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa @server_10555580\"'
     Then I should get:
       """
       $aliases["server_10555580"] = array (
         'context_type' => 'server',
         'remote_host' => '10.55.55.80',
         'http_service_type' => 'apache',
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @server_10555580 provision-verify\"'
     Then I should not get:
       """
       [error]
       """
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa @server_drupal0\"'
     Then I should get:
       """
       $aliases["server_drupal0"] = array (
         'context_type' => 'server',
         'remote_host' => 'drupal0',
         'http_service_type' => 'pack',
         'slave_web_servers' =>
         array (
           0 => '@server_web1local',
           1 => '@server_10555580',
         ),
         'master_web_servers' =>
         array (
           0 => '@server_master',
         ),
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @server_drupal0 provision-verify\"'
     Then I should not get:
       """
       [error]
       """
     When I run "vagrant suspend web1 web2"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/remove_webpack_server?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:remove_webpack_server
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       remove_webpack_server - web1_payload.json - web1.local - 10.55.55.70 - RC: 0
       remove_webpack_server - web2_payload.json - web2.local - 10.55.55.80 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       RemoveWebpackServerProcessor webhook plugin succeeded for 10.55.55.80 with
       pack cluster drupal0.
       RemoveWebpackServerProcessor webhook plugin triggered for 10.55.55.80 with
       pack cluster drupal0.
       RemoveWebpackServerProcessor webhook plugin succeeded for web1.local with pack
       cluster drupal0.
       RemoveWebpackServerProcessor webhook plugin triggered for web1.local with pack
       cluster drupal0.
       AddWebpackServerProcessor webhook plugin succeeded for 10.55.55.80 with pack
       cluster drupal0.
       Adding apache Web service for 10.55.55.80.
       AddWebpackServerProcessor webhook plugin triggered for 10.55.55.80 with pack
       cluster drupal0.
       AddWebpackServerProcessor webhook plugin succeeded for web1.local with pack
       cluster drupal0.
       Adding apache Web service for web1.local.
       AddWebpackServerProcessor webhook plugin triggered for web1.local with pack
       cluster drupal0.
       """
     Then I should not get:
       """
       Could not determine which server to remove.
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-tasks\"'
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush sa\"'
     Then I should not get:
       """
       @server_web1local
       @server_10.55.55.80
       """
