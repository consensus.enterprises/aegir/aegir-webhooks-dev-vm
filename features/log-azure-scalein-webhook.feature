@webhooks @azure @log-azure-scalein-webhook
Feature: Validate incoming Azure scale in webhook data.
  In order to test azure scale in webhooks consistently
  As a developer
  I need to log incoming Azure scale in webhook data to the database.

  Background:
    Given I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  Scenario: Call Azure scale in log webhook script.
     When I run "./scripts/server.sh log_azure_data azure_scalein_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/log_azure_data?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:log_azure_data
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       log_azure_data - azure_scalein_payload.json - localhost - 127.0.0.1 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       LogAzureDataProcessor webhook plugin succeeded for Scale In operation for the
       MyCSRole scale set.
       LogAzureDataProcessor webhook plugin triggered for Scale In operation for the
       MyCSRole scale set.
       """
