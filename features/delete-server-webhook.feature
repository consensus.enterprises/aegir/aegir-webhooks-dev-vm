@webhooks @server @delete-server-webhook
Feature: Webhook triggers deletion of an Aegir server.
  In order to delete servers in response to cloud provisioning events
  As a developer
  I need to handle delete_server webhooks properly.

  Background:
    # Clean up any cruft from previous test runs.
    Given I run "rm -f ./scripts/webhook.log"
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster php-script /vagrant/scripts/delete_servers.php\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster --yes watchdog-delete all\"'

  Scenario: Call delete_server webhook for a server that does not exist.
     When I run "./scripts/server.sh delete_server testhost_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/delete_server?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:delete_server
       * The requested URL returned error: 409 Conflict
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       delete_server - testhost_payload.json - localhost - 127.0.0.1 - RC: 22
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       Could not determine which server to delete. Error message: No server matched
       hostname.
       DeleteServerProcessor webhook plugin triggered for test.host.
       """
     Then I should not get:
       """
       DeleteServerProcessor webhook plugin succeeded for test.host.
       """

  Scenario: Call delete_server webhook for a server that was already deleted.
    Given I run 'vagrant ssh aegir --command=\"sudo -sHu aegir cp /vagrant/scripts/server_testhost.alias.drushrc.php /var/aegir/.drush\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-import @server_testhost\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-tasks\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-task @server_testhost delete\"'
      And I run 'sleep 3'
     When I run "./scripts/server.sh delete_server testhost_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/delete_server?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:delete_server
       * The requested URL returned error: 409 Conflict
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       delete_server - testhost_payload.json - localhost - 127.0.0.1 - RC: 22
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       Could not determine which server to delete. Error message: No enabled server
       matched hostname.
       DeleteServerProcessor webhook plugin triggered for test.host.
       """
     Then I should not get:
       """
       DeleteServerProcessor webhook plugin succeeded for test.host.
       """

  Scenario: Call delete_server webhook for a server that exists.
    Given I run 'vagrant ssh aegir --command=\"sudo -sHu aegir cp /vagrant/scripts/server_testhost.alias.drushrc.php /var/aegir/.drush\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-import @server_testhost\"'
      And I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster hosting-tasks\"'
      And I run 'sleep 3'
     When I run "./scripts/server.sh delete_server testhost_payload.json localhost 127.0.0.1"
     Then I should get:
       """
       * Connected to aegir.local (10.55.55.55) port 80 (#0)
       > POST /webhook/delete_server?authorization_token=
       > Host: aegir.local
       > User-Agent: Aegir-webhook/server
       > X-Aegir-Event:delete_server
       < HTTP/1.1 200 OK
       """
     When I run "tail ./scripts/webhook.log"
     Then I should get:
       """
       delete_server - testhost_payload.json - localhost - 127.0.0.1 - RC: 0
       """
     When I run 'vagrant ssh aegir --command=\"sudo -sHu aegir drush @hostmaster watchdog-show --fields=message --type=hosting_webhooks\"'
     Then I should get:
       """
       DeleteServerProcessor webhook plugin succeeded for test.host.
       DeleteServerProcessor webhook plugin triggered for test.host.
       """
     Then I should not get:
       """
       Could not determine which server to delete.
       """
# @TODO: Check that alias file is gone
