BEHAT_DEPS ?= php-curl php-mbstring php-dom
include .mk/GNUmakefile

.PHONY: all-webhooks clean-webhooks github-webhook logdata-webhook tokenauth-webhook tokenauth-webhook-notoken tokenauth-webhook-badtoken server-webhooks log-server-webhook log-server-webhook-vagrant create-server-webhook delete-server-webhook webpack-webhooks log-webpack-webhook log-webpack-webhook-vagrant add-webpack-webhook remove-webpack-webhook

all-webhooks: github-webhook logdata-webhook tokenauth-webhook server-webhooks webpack-webhooks

clean-webhooks:
	rm -f ./scripts/webhook.log
	vagrant ssh aegir --command="sudo -sHu aegir drush @hostmaster php-script /vagrant/scripts/delete_servers.php"
	vagrant ssh aegir --command="sudo -sHu aegir drush @hostmaster --yes watchdog-delete all"
	vagrant ssh aegir --command="sudo -sHu aegir rm /var/aegir/.ssh/known_hosts"

github-webhook:
	@./scripts/github.sh github github_payload.json localhost 127.0.0.1
	@tail ./scripts/webhook.log

logdata-webhook:
	@./scripts/logdata.sh example_logdata logdata_payload.json localhost 127.0.0.1
	@tail ./scripts/webhook.log

tokenauth-webhook:
	@./scripts/tokenauth.sh example_tokenauth tokenauth_payload.json localhost 127.0.0.1 90437cca-aa3b-4b07-8ce3-db95826de868
	@tail ./scripts/webhook.log
tokenauth-webhook-notoken:
	@./scripts/logdata.sh example_tokenauth tokenauth_payload.json localhost 127.0.0.1
	@tail ./scripts/webhook.log
tokenauth-webhook-badtoken:
	@./scripts/tokenauth.sh example_tokenauth tokenauth_payload.json localhost 127.0.0.1 intentionallyInvalid
	@tail ./scripts/webhook.log

server-webhooks: log-server-webhook log-server-webhook-vagrant create-server-webhook delete-server-webhook
log-server-webhook:
	@./scripts/server.sh log_server_data testhost_payload.json localhost 127.0.0.1
	@tail ./scripts/webhook.log
log-server-webhook-vagrant:
	@VAGRANT_EXPERIMENTAL="typed_triggers" vagrant status
	@tail ./scripts/webhook.log
create-server-webhook:
	@./scripts/server.sh create_server testhost_payload.json localhost 127.0.0.1
	@tail ./scripts/webhook.log
delete-server-webhook:
	@./scripts/server.sh delete_server testhost_payload.json localhost 127.0.0.1
	@tail ./scripts/webhook.log

webpack-webhooks: log-webpack-webhook log-webpack-webhook-vagrant add-webpack-webhook remove-webpack-webhook
log-webpack-webhook:
	@./scripts/server.sh log_webpack_data webpackhost_payload.json localhost 127.0.0.1
	@tail ./scripts/webhook.log
log-webpack-webhook-vagrant:
	@VAGRANT_EXPERIMENTAL="typed_triggers" vagrant status
	@tail ./scripts/webhook.log
add-webpack-webhook:
	@./scripts/server.sh add_webpack_server webpackhost_payload.json localhost 127.0.0.1 &
	@sleep 5
	@vagrant ssh aegir --command="sudo -sHu aegir drush @hostmaster hosting-tasks && sleep 5"
	@tail ./scripts/webhook.log
	@vagrant ssh aegir --command="sudo -sHu aegir drush @hostmaster hosting-tasks && sleep 5"
remove-webpack-webhook:
	@./scripts/server.sh remove_webpack_server webpackhost_payload.json localhost 127.0.0.1
	@tail ./scripts/webhook.log
	@vagrant ssh aegir --command="sudo -sHu aegir drush @hostmaster hosting-tasks && sleep 5"

process-queue:
	vagrant ssh aegir --command="sudo -sHu aegir drush @hostmaster hosting-tasks && sleep 5"


