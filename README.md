Aegir Development VM
====================

This project is intended to provide a simple Vagrant-based VM for hacking on
Aegir (or simply trying it).

![](images/sites-screenshot.png)

Getting Started
---------------

To get started, install [Vagrant](https://www.vagrantup.com/docs/installation/)
and [Virtualbox](https://www.virtualbox.org/wiki/Downloads), then run:

```
git clone --recursive https://gitlab.com/aegir/aegir-dev-vm
cd aegir-dev-vm
vagrant up             # Launch the VM, and install Aegir
vagrant ssh            # SSH into the VM
sudo -sHu aegir        # Become the `aegir` user
drush @hm uli          # Generate a one-time login link
```

To access the Aegir front-end, you'll need to add an entry in your `/etc/hosts`:

```
10.55.55.55 aegir.local
```

DigitalOcean Integration
------------------------

You can also [run your Vagrant box on DigitalOcean](./digital_ocean/README.md).
