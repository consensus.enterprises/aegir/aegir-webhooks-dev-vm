<?php 
$aliases['server_webpackhost'] = array (
  'context_type' => 'server',
  'server' => '@server_master',
  'remote_host' => 'webpack.host',
  'aegir_root' => '/var/aegir',
  'script_user' => 'aegir',
  'ip_addresses' => 
  array (
  ),
  'backup_path' => '/var/aegir/backups',
  'config_path' => '/var/aegir/config/server_webpackhost',
  'include_path' => '/var/aegir/config/includes',
  'clients_path' => '/var/aegir/clients',
  'master_url' => 'http://aegir.local/',
  'admin_email' => 'admin@webpack.host',
);
