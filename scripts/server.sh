#!/bin/bash

# Get this script's path
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
DATESTAMP=`date --rfc-3339=seconds`

webhook=$1
payload=$2
hostname=$3
ip_address=$4
token=90437cca-aa3b-4b07-8ce3-db95826de868

curl http://aegir.local/webhook/$webhook?authorization_token=$token \
  --silent \
  --verbose \
  --insecure \
  --output /dev/null \
  --fail \
  --show-error \
  --user-agent "Aegir-webhook/server" \
  --Header "Content-Type:application/json" \
  --Header "X-Aegir-Event:$webhook" \
  --data "@$DIR/$payload"

echo "$DATESTAMP - $webhook - $payload - $hostname - $ip_address - RC: $?" >> $DIR/webhook.log
