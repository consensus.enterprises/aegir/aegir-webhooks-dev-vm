<?php

/**
 * Delete all test server nodes.
 */

if (exec('whoami') != 'aegir') {
  drush_print_r('This script must only be run as the aegir user.');
  die(1);
}

// Reset default.
variable_set('hosting_webhooks_check_ssh', TRUE);
variable_set('hosting_webhooks_check_ssh_timeout', 30);
variable_set('hosting_webhooks_verify_timeout', 60);
variable_get('hosting_webhooks_scaleset_timeout', 30);

$servers = [
  'test.host' => 'server_testhost',
  'services.host' => 'server_serviceshost',
  'webpack.host' => 'server_webpackhost',
  'web0.local' => 'server_web0local',
  'web1.local' => 'server_web1local',
  '10.55.55.80' => 'server_10555580',
  'drupal0' => 'server_drupal0',
  'drupal-webpack' => 'server_drupalwebpack',
];

foreach ($servers as $server => $alias) {
  $conditions = [
    'title' => $server,
    'type' => 'server',
  ];
  $nodes = node_load_multiple(NULL, $conditions);

  foreach ($nodes as $nid => $node) {
    node_delete($nid);
    drush_print_r('Deleted server node: ' . $server . ' (' . $nid . ')');
  }

  $alias_path = '/var/aegir/.drush/' . $alias . '.alias.drushrc.php';
  if (file_exists($alias_path)) {
    drush_print_r('Deleting alias : ' . $alias_path);
    unlink($alias_path);
  }

}
