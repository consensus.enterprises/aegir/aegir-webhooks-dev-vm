#!/bin/bash

# Get this script's path
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
DATESTAMP=`date --rfc-3339=seconds`

webhook=$1
payload=$2
hostname=$3
ip_address=$4

curl http://aegir.local/webhook/$webhook \
  --silent \
  --verbose \
  --insecure \
  --output /dev/null \
  --fail \
  --show-error \
  --user-agent "GitHub-Hookshot/github" \
  --Header "Content-Type:application/json" \
  --Header "X-Github-Event:push" \
  --data "@$DIR/$payload"

echo "$DATESTAMP - $webhook - $payload - $hostname - $ip_address - RC: $?" >> $DIR/webhook.log
