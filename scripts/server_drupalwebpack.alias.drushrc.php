<?php 
$aliases['server_drupalwebpack'] = array (
  'context_type' => 'server',
  'server' => '@server_master',
  'remote_host' => 'drupal-webpack',
  'aegir_root' => '/var/aegir',
  'script_user' => 'aegir',
  'ip_addresses' => 
  array (
  ),
  'backup_path' => '/var/aegir/backups',
  'config_path' => '/var/aegir/config/server_drupal0',
  'include_path' => '/var/aegir/config/includes',
  'clients_path' => '/var/aegir/clients',
  'master_url' => 'http://aegir.local/',
  'admin_email' => 'admin@drupal-webpack',
  'http_service_type' => 'pack',
  'slave_web_servers' => 
  array (
    0 => '@server_webpackhost',
  ),
  'master_web_servers' => 
  array (
    0 => '@server_master',
  ),
);
