SSH_PRIVKEY=.secret/aegir_id_rsa
SSH_PUBKEY=$(SSH_PRIVKEY).pub
AZURE_SECRET ?= .secret/azure-secret.json
AEGIR_SERVER_NAME ?= aegir0
AEGIR_IMAGE_NAME ?= aegirImage 

$(AZURE_SECRET):
	$(error You need an Azure secret file; please see README.md)

aegir-image: $(AZURE_SECRET)
	@echo "(Re)building Aegir image on Azure."
	@packer build -force -var-file $(AZURE_SECRET) aegir-image.json

minion-image: $(AZURE_SECRET)
	@echo "(Re)building minion image on Azure."
	@packer build -force -var-file $(AZURE_SECRET) minion-image.json

scaleset: $(AZURE_SECRET)
	@echo "Instantiating scaleset."
	@az vmss create --resource-group aegirWebhookTest --name aegirWebhookTestScaleSet --image minionImage --ssh-key-values .secret/aegir_id_rsa.pub --location canadaeast --admin-username az --public-ip-per-vm

provision-aegir-server: $(AZURE_SECRET)
	@echo "Instantiating Aegir image on Azure."
	@az vm create --resource-group aegirWebhookTest  --image $(AEGIR_IMAGE_NAME) --name $(AEGIR_SERVER_NAME) --admin-username az --ssh-key-values ~/.ssh/id_rsa.pub

configure-aegir-server: $(AZURE_SECRET)
	@IP=`az vm list-ip-addresses --resource-group aegirWebhookTest --name $(AEGIR_SERVER_NAME) | jq '.[0].virtualMachine.network.publicIpAddresses[0].ipAddress')` && printf "Host $(AEGIR_SERVER_NAME)\n  Hostname $$IP\n  user az\n" > ansible/ssh-aegir.config
	@cat ansible/ssh-aegir.config
	@cd ansible && ssh -F ssh-aegir.config -o StrictHostKeyChecking=no aegir0 'sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 1'
	@cd ansible && ansible-playbook --ssh-extra-args "-o StrictHostKeyChecking=no -F ssh-aegir.config" -e aegir_stop_before_site_install=False --inventory $(AEGIR_SERVER_NAME), aegir-image.yml

aegir-server: $(AEGIR_SECRET) provision-aegir-server configure-aegir-server

clean-aegir-server: $(AZURE_SECRET)
	@az vm delete --resource-group aegirWebhookTest --name $(AEGIR_SERVER_NAME)

clean-scaleset: $(AZURE_SECRET)
	@az vmss delete --resource-group aegirWebHookTest --name aegirWebhookTestScaleSet

clean: clean-aegir-server clean-scaleset
	@echo "Removing aegir key pair, if it exists."
	@rm -f $(SSH_PRIVKEY) $(SSH_PUBKEY)
