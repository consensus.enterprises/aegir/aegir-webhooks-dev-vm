# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/bionic64"

  # Default to small VMs (for the minions)
  config.vm.provider "virtualbox" do |v|
    v.memory = 1024
    v.cpus = 2
  end

  # Workarounds for:
  # https://github.com/hashicorp/vagrant/issues/10914
  # https://github.com/hashicorp/vagrant/issues/10950
  $script = <<-SCRIPT
  if [[ (( `which ansible` )) ]]; then
    exit 0
  fi
  echo Implementing work-around for Ansible install hanging.
  sudo apt-get update -y -qq
  sudo DEBIAN_FRONTEND=noninteractive apt-get remove -y -qq --option \"Dpkg::Options::=--force-confold\" python
  sudo DEBIAN_FRONTEND=noninteractive apt-get install -y -qq --option \"Dpkg::Options::=--force-confold\" libssl1.1 python3-pip

  SCRIPT

  config.vm.provision "shell", inline: $script

  config.vm.provision "ansible_local" do |ansible|
    ansible.install_mode = "pip3"

    ansible.playbook = "ansible/minion-playbook.yml"
    ansible.groups = {
      "aegir" => ["aegir"],
      "minions" => ["web[0:2]"]
    }
  end

  config.vm.define "aegir", primary: true do |aegir|
    aegir.vm.hostname = 'aegir.local'
    aegir.vm.network 'private_network', ip: '10.55.55.55'

    # Make the hostmaster VM more robust.
    aegir.vm.provider "virtualbox" do |v|
      v.memory = 4096
      v.cpus = 4
    end

    aegir.vm.provision "ansible_local", playbook: "ansible/aegir-playbook.yml"
  end

  ## Test for server creation and deletion.

  servers = {
    "web0" => { "hostname" => "web0.local", "ip" => "10.55.55.60" },
  }

  servers.each do | name, info |

    config.vm.define "#{name}" do | server |
      server.vm.hostname = "#{info['hostname']}"
      server.vm.network 'private_network', ip: "#{info['ip']}"
    end

    config.trigger.after [:up, :resume] do |t|
      t.name = 'Send a "Scale Out" webhook.'
      t.only_on = ["#{name}"]
      t.run = {
        path: "scripts/server.sh",
        args: ["create_server", "#{name}_payload.json", "#{info['hostname']}", "#{info['ip']}"]
      }
    end

    config.trigger.before [:destroy, :halt, :suspend] do |t|
      t.name = 'Send a "Scale In" webhook.'
      t.only_on = ["#{name}"]
      t.run = {
        path: "scripts/server.sh",
        args: ["delete_server", "#{name}_payload.json", "#{info['hostname']}", "#{info['ip']}"]
      }
    end

    config.trigger.after :status, type: :command do |t|
      t.name = 'Trigger webhook to log server data.'
      t.only_on = ["#{name}"]
      t.run = {
        path: "scripts/server.sh",
        args: ["log_server_data", "#{name}_payload.json", "#{info['hostname']}", "#{info['ip']}"]
      }
    end

  end

  ## Test for adding servers to a webpack and removing them.

  minions = {
    "web1" => { "hostname" => "web1.local", "ip" => "10.55.55.70" },
    "web2" => { "hostname" => "web2.local", "ip" => "10.55.55.80" },
  }

  minions.each do | name, info |

    config.vm.define "#{name}" do | minion |
      minion.vm.hostname = "#{info['hostname']}"
      minion.vm.network 'private_network', ip: "#{info['ip']}"
    end

    config.trigger.after [:up, :resume] do |t|
      t.name = 'Send a "Scale Out" webhook.'
      t.only_on = ["#{name}"]
      t.run = {
        path: "scripts/server.sh",
        args: ["add_webpack_server", "#{name}_payload.json", "#{info['hostname']}", "#{info['ip']}"]
      }
    end

    config.trigger.before [:destroy, :halt, :suspend] do |t|
      t.name = 'Send a "Scale In" webhook.'
      t.only_on = ["#{name}"]
      t.run = {
        path: "scripts/server.sh",
        args: ["remove_webpack_server", "#{name}_payload.json", "#{info['hostname']}", "#{info['ip']}"]
      }
    end

    config.trigger.after :status, type: :command do |t|
      t.name = 'Trigger webhook to log server data.'
      t.only_on = ["#{name}"]
      t.run = {
        path: "scripts/server.sh",
        args: ["log_server_data", "#{name}_payload.json", "#{info['hostname']}", "#{info['ip']}"]
      }
    end

  end

end
